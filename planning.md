Django project --> Tracker --> MAIN_PROJECT
3 apps -- > tasks
       -- > projects
       -- > accounts
superuser   -- > curtischeung
            -- > password : password
#########################################################################
(pass) Feature 1 -- > installed (pass) + 5 points
    - django
    - black
    - flake8
    - djhtml
#########################################################################
(pass) Feature 2 -- > created -> tasks -> projects -> accounts (pass) + 7 points
    - INSTALLED into settings.py under INSTALLED APPS
#########################################################################
(pass) Feature 3 --> Model in Projects named Project (pass) + 12 points

    ->name -> string(max_length of 200) -> (CharField)
    ->description -> string(nolimit) -> (TextField)
#########################################################################
(pass) Feature 4 --> Register Project model into admin (pass) + 1 point
#########################################################################
(pass) Feature 5 --> Create ListView for the Project Model (pass) + 9 points

    - register view path
        -> "" with name "list_projects" in new file named projects/urls.py
    - register project path with tracker project
        -> use prefix "projects/"
    - create a template for list view
        -> template 
            - fundamental 5
            - <main>
            - <div> with <h1>My Projects</h1>
            - if no projects --> <p>You are not ssigned to any projects"</p>
            - otherwise --> a table with two columns
                -> "Name" with rows with name of projects
                -> "Number of tasks" with nothing 
#########################################################################
(pass) Feature 6 --> tracker urls.py use (pass) + 2 points

    - RedirectView to redirect from "" to name of path for list view
    - from django.views.generic.base import RedirectView
    - Register the path as "home"
#########################################################################
(pass) Feature 7 --> Login Page For people to use the application (pass) + 10 points

    - Register LoginView in accounts urls.py -- > path "login/" and name "login"
    - Include URL patterns from accounts app in tracker project with prefix "accounts/"
    - Create templates directory under accounts
    - Create registration directory under templates
    - Create an HTML template named login.html in registration directory
    - In tracker settings.py
        --> create and set variable LOGIN_REDIRECT_URL --> to the value "home" 
            --> redirect to the path (not yet created) with the name "home"
    --> Template Specifications
        - fundamental 5
        -main tag containing
            - div tag containing
                -h1 - login -h1-
                - a form tag- method POST with any kind of HTML structures
                    --> structues must include
                        - input tag with type "text" and name "username"
                        - input tag with type "password: and name "password"
                        - a button with content "Login"
        ----> http://localhost:8000/accounts/login/ <----
#########################################################################
(pass) Feature 8 --> Protecting the list view for the Projet model so that a person that has logged in can access it (pass)

          --> Change queryset of the view to filter Project objects where members equals the logged in user
#########################################################################
(pass) Feature 9 -- > In the accounts/urls.py (pass)

            - import LogoutView from the same module that you imported   the LoginView
            - register the view in the urlpatterns list with the path "logout/" and name logout
            -In tracker settings.py create and set the Logout_Redirect_URL to the value "Login"

    path("accounts/login/", auth_views.LoginView.as_view(), name="login"),
    path("accounts/logout/", auth_views.LogoutView.as_view(), name="logout"),
#########################################################################
(pass) Feature 10 --> Signup localhost:8000/accounts/signup/ (pass) + 10 points

    -Function View to handle sign up form
    -Created in the accounts directory - in views

    - UserCreationForm from buil-in auth forms
    - Special create_user method - username / password
    - Login function that logs an account in
    - After creating user, redirect browser to the path registered "home"
    - Create HTML template named signup.html in registration directory
    - Put POST form in the signup.html with required HTML or template inheritance

    Template Specifications
    - Fundamental 5
    - main tag containing 
        div tag
            - h1 Signup/h1
            - form tag with method "post" containing any kind of html
            - input tag with type "text" with name "username"
            - input tag with type "password" and name "password1"
            - input tag  with type "password" and name "password2"
            - buutton with content Signup"

#########################################################################
(pass) Feature 11 --> Create A Task Model in tasks Django app (pass) + 20 points

    Task model
    - name --> string 200 char
    - start_date --> date-time
    - due_date --> date-time
    - is_completed --> Boolean --> default False
    - project --> ForeignKey --> project.Project model related_name =   "tasks" with cascade on delete
    - assignee - ForeignKey --> auth.User Model null is True, related_name- "tasks" on delete set null
    -should have __str__ method

#########################################################################
(pass) Feature 12 --> Register Task Model in Admin (pass) + 1 point
    - admin.site.register(Task)

#########################################################################
Feature 13 --> Allows people to see details about a project

    --> Create a View that shows details of a particular project
    --> User must be logged in to see the view
    --> In projects urls.py register the view with path
            - path("<int:pk>/" and the name "show_project)
    --> Create a template to show the details of the project and a table of its tasks
    --> Update the list template to show the number of tasks for a project
    --> Update the list template to have a link from the project name to the detail view for that project

    Template Specifications
    - Fundamental 5
    - main tag with a div tag
    - div tag includes 
    - h1 - {{project.Project.Name}}
    - p - tag with description in it
    - h2 - with content  "Tasks"
        if project has tasks then
            a table contains five columns with headers
                - Name
                - Assignee
                - Start date
                - Due date
                - Is completed
            with rows for each task in the project
        otherwise
            a p tag with content "This project has no tasks"

  * Note * : the "Is completed" column shows "yes and no for the task is_completed status

#########################################################################
  (pass) Feature 14 - Create Task View - (pass_)
    -model Task
    -template_name task_create.html
    -fields
        -name
        -start_date
        -due_date
        -projects
        -assignee
    -make sure all fields have _ for spaces*
#########################################################################
Feature 15 -