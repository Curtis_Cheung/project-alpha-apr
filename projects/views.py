from django.views.generic.list import ListView
from .models import Project
from django.views.generic.base import RedirectView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "project_list.html"

    def get_queryset(self):
        # import pdb
        # pdb.set_trace()
        return Project.objects.filter(members=self.request.user)


class ProjectRedirectView(RedirectView):
    url = "/projects/"
    pattern_name = "home"


class ProjectDetailView(DetailView):
    model = Project
    template_name = "project_details.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "project_create.html"
    fields = ["name", "description", "members"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
